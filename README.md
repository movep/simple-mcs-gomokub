Image source from:
https://sourceforge.net/projects/cutegomoku/

==Rules==
1. Five or more stones to win.
2. No ban move.

==How to play this AI==

1. Download piskvork
http://petr.lastovicka.sweb.cz/piskvork.zip

2. Unzip and Open piskvork

3. Set AI to "pbrain-mcs.exe"
http://gomocup.org/download-gomocup-manager/

Chinese readme below:

使用說明：
五子棋是個經典遊戲，小弟我寫了個簡單規則的五子棋AI,
不像市面上的商業AI, 我寫的棋力算弱的，
但我有時候也是會輸給自己寫的AI, 個人覺得這樣有輸有贏比較好玩。
簡單規則就是連五子或以上，都算勝利，並且沒有任何禁手，
GUI是使用第三方軟体，我只寫AI的部份。由於我只支援簡單規則，
所以由GUI更改規則的話，雖然我沒試過，但一定是有問題的。

遊玩方法：
1. 下載回來解開壓縮
2. Open piskvork.exe
3. 參考README.png設定AI為pbrain-mcs.exe