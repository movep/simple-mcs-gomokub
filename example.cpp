#include "pisqpipe.h"
#include <windows.h>
#include <cstdio>
#include <cstdlib>
#include <conio.h>
#include <ctime>
#include <cstring>

# define MCS_num 4500

const char *infotext="name=\"MCS\", author=\"Simon Chen\", version=\"0.1\", country=\"Taiwan\", www=\"https://gitlab.com/movep/simple-mcs-gomokub\"";

#define MAX_BOARD 100
int board[MAX_BOARD][MAX_BOARD];
static unsigned seed;

int select_mark[MAX_BOARD][MAX_BOARD] = {-1}; // ini = -1
int select_p[MAX_BOARD*MAX_BOARD] = {0};
//int rs[MAX_BOARD][MAX_BOARD] = {0};
int ssize = 0; // the size of select array
int pnum = 0; // the number of pieces

struct cor{
	int i;
	int j;
}com_ai;

unsigned rnd(unsigned n);

int random_pickup(int sel[MAX_BOARD*MAX_BOARD], int size);
int add_select(int x, int y, int sel[MAX_BOARD*MAX_BOARD], int size, int sel_mark[MAX_BOARD][MAX_BOARD], int bd[MAX_BOARD][MAX_BOARD]);
int seq_num(int mark[MAX_BOARD][MAX_BOARD], int dir, int x, int y, int pcolor, int & live, int bd[MAX_BOARD][MAX_BOARD]);
int gom_win(int x, int y, int pcolor, int bd[MAX_BOARD][MAX_BOARD]);

int random_pickup(int sel[MAX_BOARD*MAX_BOARD], int size){
	
	int r;
    
	//sed ++;
	//srand(sed);
    
    if(size > 0){
        r = rnd(size);
    }
    else{
        pipeOut("ERROR!!! mod 0\n");
    }
	
	com_ai.i = sel[r] / width;
	com_ai.j = sel[r] % width;

	return r;
}

int add_select(int x, int y, int sel[MAX_BOARD*MAX_BOARD], int size, int sel_mark[MAX_BOARD][MAX_BOARD], int bd[MAX_BOARD][MAX_BOARD]){
	int i, j;
	
	for (i=y-2;i<=y+2;i++){
		for (j=x-2;j<=x+2;j++){
			if(i < 0 || j < 0 || i >= height || j >= width)continue;
			else if(i == y && j == x)continue;
			if (0 == bd[i][j] && -1 == sel_mark[i][j]) {
				//rs[i][j] = size;
                sel_mark[i][j] = size;
                sel[size++] = i*width + j;                
			}
		}
	}
    
    return size;
}

//return 1, 2: pcolor win, 0: NOT win
int gom_win(int x, int y, int pcolor, int bd[MAX_BOARD][MAX_BOARD]){
	int mark[MAX_BOARD][MAX_BOARD] = {0};
	int i = 0;
    int live = 0;
	int seq = 0;
	int dir = 0;
    
    memset(mark, 0, sizeof(mark));
    
	while(dir < 4){ // End at dir = 4 or 5
		live = 0;
		seq = seq_num(mark, dir, x, y, pcolor, live, bd);
        // temp
        //printf("dir = %d, seq = %d, live = %d\n", dir, seq, live); 
        // end temp
		if (seq > 4)return pcolor;
		else if (4 == seq && 2 == live)return pcolor;
		else if (4 == seq && 1 == live){
			dir ++;
			while(dir < 4){
				live = 0;
				memset(mark, 0, sizeof(mark));
				seq = seq_num(mark, dir, x, y, pcolor, live, bd);
				if (seq > 4)return pcolor;
				else if (4 == seq && 2 == live)return pcolor;
				else if (4 == seq && 1 == live)return pcolor;
				else if (3 == seq && 2 == live)return pcolor;
				dir ++;
			}			
		}
		else if (3 == seq && 2 == live){
			dir ++;
			while(dir < 4){
				live = 0;
				memset(mark, 0, sizeof(mark));
				seq = seq_num(mark, dir, x, y, pcolor, live, bd);
				if (seq > 4)return pcolor;
				else if (4 == seq && 2 == live)return pcolor;
				else if (4 == seq && 1 == live)return pcolor;
				//else if (3 == seq && 2 == live)return pcolor;
				dir ++;
			}
		}
		dir ++; // dir may 4, after dir ++, it will be 5
		memset(mark, 0, sizeof(mark));
	}
	return 0;
}
	
// dir = 0: x, 1: top left, 2: y, 3: top right
// pcolor = 1: black, 2: white
// return: pieces sequence		
int seq_num(int mark[MAX_BOARD][MAX_BOARD], int dir, int x, int y, int pcolor, int & live, int bd[MAX_BOARD][MAX_BOARD]){
	
    if(x >= width || y >= height || x < 0 || y < 0){
        return 0;
    }
	else if(1 == mark[y][x]){
		return 0;
	}
	else{
		mark[y][x] = 1;
		if(bd[y][x] != pcolor){
            if(0 == bd[y][x]) live ++;
                return 0;
		}
		else{
			//x
			if(0 == dir) return seq_num(mark, dir, x+1, y, pcolor, live, bd) + 1 + seq_num(mark, dir, x-1, y, pcolor, live, bd);
			//top left
			else if(1 == dir) return seq_num(mark, dir, x-1, y-1, pcolor, live, bd) + 1 + seq_num(mark, dir, x+1, y+1, pcolor, live, bd);
			//y
			else if(2 == dir) return seq_num(mark, dir, x, y+1, pcolor, live, bd) + 1 + seq_num(mark, dir, x, y-1, pcolor, live, bd);
			//top right
			else if(3 == dir) return seq_num(mark, dir, x+1, y-1, pcolor, live, bd) + 1 + seq_num(mark, dir, x-1, y+1, pcolor, live, bd);
			else{
				printf("error");
				return -255;
			}
		}
	}
}

void brain_init() 
{
  memset(select_mark, -1, sizeof(select_mark));// Simon added
  memset(select_p, 0, sizeof(select_p));
  ssize = 0; // the size of select array
  pnum = 0;
    
  if(width<5 || height<5){
    pipeOut("ERROR size of the board");
    return;
  }
  if(width>MAX_BOARD || height>MAX_BOARD){
    pipeOut("ERROR Maximal board size is %d", MAX_BOARD);
    return;
  }
  seed=start_time;
  pipeOut("OK");
}

void brain_restart()
{
  int x,y;
  for(y=0; y<height; y++){
    for(x=0; x<width; x++){
      board[y][x]=0;
    }
  }
  memset(select_mark, -1, sizeof(select_mark));
  memset(select_p, 0, sizeof(select_p));
  //memset(rs, -1, sizeof(rs));
  ssize = 0; // the size of select array
  pnum = 0;
  
  pipeOut("OK");
}

int isFree(int x, int y)
{
  return x>=0 && y>=0 && x<width && y<height && board[y][x]==0;
}

void brain_my(int x,int y)
{
    int ti, tj, org_rs;
	if(pnum > 0){
        if(select_mark[y][x] > -1){
            if( select_mark[y][x] < ssize - 1){
                org_rs = select_mark[y][x];
                select_p[select_mark[y][x]] = select_p[ssize - 1];
                ti = select_p[select_mark[y][x]] / width;
                tj = select_p[select_mark[y][x]] % width;
                select_mark[ti][tj] = org_rs;
            }
            select_mark[y][x] = -1;
            ssize --;
        }
  	}
  	ssize = add_select(x, y, select_p, ssize, select_mark, board);
  	pnum ++;
  	if(isFree(x,y)){
    	board[y][x]=1;
  	}else{
    	pipeOut("ERROR my move [%d,%d]",x,y);
  	}
}

void brain_opponents(int x,int y) 
{
    int ti, tj, org_rs;
  	if(pnum > 0){
        if(select_mark[y][x] > -1){
            if( select_mark[y][x] < ssize - 1){
                org_rs = select_mark[y][x];
                select_p[select_mark[y][x]] = select_p[ssize - 1];
                ti = select_p[select_mark[y][x]] / width;
                tj = select_p[select_mark[y][x]] % width;
                select_mark[ti][tj] = org_rs; 		
            }
            select_mark[y][x] = -1;
            ssize --;
        }
  	}
  	ssize = add_select(x, y, select_p, ssize, select_mark, board);
  	pnum ++;
  	if(isFree(x,y)){
    	board[y][x]=2;
  	}else{
    	pipeOut("ERROR opponents's move [%d,%d]",x,y);
  	}
}

void brain_block(int x,int y)
{
  if(isFree(x,y)){
    board[y][x]=3;
  }else{
    pipeOut("ERROR winning move [%d,%d]",x,y);
  }
}

int brain_takeback(int x,int y)
{
  if(x>=0 && y>=0 && x<width && y<height && board[y][x]!=0){
    board[y][x]=0;
    return 0;
  }
  return 2;
}

unsigned rnd(unsigned n)
{
  seed=seed*367413989+174680251;
  return (unsigned)(UInt32x32To64(n,seed)>>32);
}

void brain_turn() 
{
	//unsigned sed;
    //bool first = true;
	int ii, jj, r, human_color, temp, fx, fy, fpx = 0, fpy = 0, limit;
	int win = 0; //0 : Not know, 1: black, 2: white
    int score[MAX_BOARD][MAX_BOARD] = {0};
    int score_opp[MAX_BOARD][MAX_BOARD] = {0};
    int snum[MAX_BOARD][MAX_BOARD] = {0}; // sel size
    //int snum_opp[MAX_BOARD][MAX_BOARD] = {0}; 
    int sel[MAX_BOARD*MAX_BOARD] = {0};
    int size = ssize;
    int sel_m[MAX_BOARD][MAX_BOARD] = {0};
    int ai_pnum = pnum;
    int com_color = 1; //1: my, 2: opp
    int org_color = 1; //1: my, 2: opp
    int bd[MAX_BOARD][MAX_BOARD] = {0};
    int mark[MAX_BOARD][MAX_BOARD] = {0};
    int live = 0;
    int seq = 0;
    int dir = 0;
    int bias = 0;
    
    //int winp[MAX_BOARD][MAX_BOARD] = {0};
    int maxp = -1;
    //int winopp[MAX_BOARD][MAX_BOARD] = {0};
    int maxopp = -1;
    
    memset(score, 0, sizeof(score));
    memset(score_opp, 0, sizeof(score_opp));
    memset(snum, 0, sizeof(snum));
    //memset(snum_opp, 0, sizeof(snum_opp));
    memcpy(bd, board, sizeof(board));
    
    
    
    if(0 == pnum){
    	do_mymove(width/2, height/2);
	}
    else{
        
       for(ii = 0; ii < height; ii++){
            for(jj = 0; jj < width; jj++){
                if(0 == bd[ii][jj]){                    
                    dir = 0;
                    bd[ii][jj] = 1; //my
                    while(dir < 4){
                        memset(mark, 0, sizeof(mark));
                        live = 0;
                        seq = seq_num(mark, dir, jj, ii, 1, live, bd);
                        if(seq > 4){
                            //printf("win 5\n");
                            do_mymove(jj, ii);
                            return;
                        }
                        dir ++;
                    }
                    
                    bd[ii][jj] = 0; //back to null
                }
            }
        }
        
        for(ii = 0; ii < height; ii++){
            for(jj = 0; jj < width; jj++){
                if(0 == bd[ii][jj]){                                       
                    dir = 0;
                    bd[ii][jj] = 2; //opp
                    while(dir < 4){
                        memset(mark, 0, sizeof(mark));
                        live = 0;
                        seq = seq_num(mark, dir, jj, ii, 2, live, bd);
                        if(seq > 4){
                            //printf("def 5\n");
                            do_mymove(jj, ii);
                            return;
                        }
                        dir ++;
                    }
                    
                    bd[ii][jj] = 0; //back to null
                }
            }
        }
        
	  //sed = time(NULL);
	  for(ii=0; ii < MCS_num; ii++){
          memcpy(bd, board, sizeof(board));
          memcpy(sel_m, select_mark, sizeof(select_mark));
          memcpy(sel, select_p, sizeof(select_p));
          size = ssize;
          limit = 0;
          ai_pnum = pnum;
          win = 0;
          com_color = org_color;
          human_color = (2 == com_color) ? 1 : 2;

		  while(ai_pnum < height*width && 0 == win && limit < 100){
			r = random_pickup(sel, size);
            //printf("r = %d\n", r);
            if (0 == limit){// first
                fy = com_ai.i;
                fx = com_ai.j;
                //select_mark[fy][fx] = r;
                
                //first = false;
            }
            /*
            else if(1 == limit){// second
                fpy = com_ai.i;
                fpx = com_ai.j;
            }
            */

            bd[com_ai.i][com_ai.j] = com_color;
            win = gom_win(com_ai.j, com_ai.i, com_color, bd);
            //printf("win = %d\n", win);//temp
            
			if (win != 0){ // someone win
			    if(ai_pnum == pnum && org_color == win) //first
                {
                	score[com_ai.i][com_ai.j] += 1000;
			    }
                else if(org_color == win){
                    score[com_ai.i][com_ai.j] ++;
                }
                else score_opp[com_ai.i][com_ai.j] ++;
                //printf("score = %d, fx = %d, fy = %d\n", score[fy][fx], fx, fy);
                break;
            }
            
			ai_pnum ++;           
			            
            // remove and add select
            if( r < size - 1){
                sel[r] = sel[size - 1];
            }
            size --;
            size = add_select(com_ai.j, com_ai.i, sel, size, sel_m, bd);
            //end remove and add select
            
            //swap com_color and human_color
            temp = com_color;
            com_color = human_color;
            human_color = temp;
            
            limit ++;
		  }
          
          snum[fy][fx] = 1;
          
          //first = true;
	  }

      for(ii=0; ii<height; ii++){
          for(jj=0; jj<width; jj++){
              if(snum[ii][jj] > 0){
                  //winp[ii][jj] = score[ii][jj];
                  if(score[ii][jj] > maxp){
                    maxp = score[ii][jj];
                    // reuse fy, fx to final select
                    fy = ii;
                    fx = jj;
                    //printf("maxp = %f, fx = %d, fy = %d\n", maxp, fx, fy);//temp
                  }
              }
          }
      }
      
      for(ii=0; ii<height; ii++){
          for(jj=0; jj<width; jj++){
              if(snum[ii][jj] > 0){
                  //winopp[ii][jj] = score_opp[ii][jj];
                  if(score_opp[ii][jj] > maxopp){
                    maxopp = score_opp[ii][jj];
                    // reuse fy, fx to final select
                    fpy = ii;
                    fpx = jj;
                    //printf("maxp = %f, fx = %d, fy = %d\n", maxp, fx, fy);//temp
                  }
              }
          }
      }
      
      bias = MCS_num / 500;
      
      if(maxopp  + bias > maxp){
          fy = fpy;
          fx = fpx;
      }
    
      do_mymove(fx,fy);
    }
    //return fy * width + fx;
/*
  int x,y,i;

  i=-1;
  do{
    x=rnd(width);
    y=rnd(height);
    i++;
    if(terminateAI) return;
  }while(!isFree(x,y));

  if(i>1) pipeOut("DEBUG %d coordinates didn't hit an empty field",i);
  do_mymove(x,y);
*/
}

void brain_end()
{
}

#ifdef DEBUG_EVAL
#include <windows.h>

void brain_eval(int x,int y)
{
  HDC dc;
  HWND wnd;
  RECT rc;
  char c;
  wnd=GetForegroundWindow();
  dc= GetDC(wnd);
  GetClientRect(wnd,&rc);
  c=(char)(board[y][x]+'0');
  TextOut(dc, rc.right-15, 3, &c, 1);
  ReleaseDC(wnd,dc);
}

#endif
